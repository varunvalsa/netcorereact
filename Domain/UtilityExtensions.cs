﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public static  class UtilityExtensions
    {
        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static T To<T>(this string json)
        {
            //JsonSerializerSettings settings = new JsonSerializerSettings { ObjectCreationHandling = ObjectCreationHandling.Replace };
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static string AttributeValue<Enum, TAttribute>(this Enum value, Func<TAttribute, string> func) where TAttribute : Attribute
        {
            FieldInfo field = value.GetType().GetField(value.ToString());

            //TAttribute attribute = Attribute.GetCustomAttribute(field, typeof(TAttribute)) as TAttribute;
            // return attribute == null ? value.ToString() : func(attribute);
            return !(Attribute.GetCustomAttribute(field, typeof(TAttribute)) is TAttribute attribute) ? value.ToString() : func(attribute);

        }

        public static string StringShort(this string value, int maxLength)
        {
            return value?.Substring(0, Math.Min(value.Length, maxLength))+"...";
        }
    }
}
