﻿using Abstract;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Newtonsoft.Json;
using System;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public sealed class DbRepository : IDbRepository
    {
        private readonly SqlDatabase database;
        public DbRepository(Database database)
        {
            this.database = database as SqlDatabase;
        }



        public async Task<DbResponseBase<TResult>> GetResponseAsync<TResult, T>(DbRequestBase<T> request) where TResult : class
        {
            DbCommand cmd = database.GetStoredProcCommand(request.ProcedureName);

            SpParameterMapper parameterMapper = new SpParameterMapper();
            parameterMapper.AssignParameters(cmd, new object[] { request.InputJson });

            DbJsonResponseMapper responseMapper = null;
            TResult model = default(TResult);
            try
            {
                #region Execute Stored Procedure.
                responseMapper = await Task.Run(() =>
                                 database
                                     .ExecuteSprocAccessor<DbJsonResponseMapper>(
                                         request.ProcedureName,
                                         parameterMapper,
                                         new object[] { request.InputJson.ToJson() }
                                     ).SingleOrDefault()
                             );
                #endregion



            }
            catch (ArgumentNullException ex1) {
                return new DbResponseBase<TResult>
                {
                    Data = null,
                    Status = false,
                    RetVal = 0,
                    ResponseMessage = ex1.Message,
                    ErrorNumber = -10
                };
            }
            catch (ArgumentException ex2) {
                return new DbResponseBase<TResult>
                {
                    Data = null,
                    Status = false,
                    RetVal = 0,
                    ResponseMessage = ex2.Message,
                    ErrorNumber = -10
                };
            }
            catch (Exception ex) {
                return new DbResponseBase<TResult>
                {
                    Data = null,
                    Status = false,
                    RetVal = 0,
                    ResponseMessage = ex.Message,
                    ErrorNumber = -10
                };
            }

            OutputParameter output = parameterMapper.GetOurputParameters();

            #region Prepare Result based on Output Parameter.
            model = responseMapper.Data?.To<TResult>();
            #endregion
            return new DbResponseBase<TResult>
            {
                Data = model,
                Status = true,
                RetVal = output.RetVal,
                ResponseMessage = output.ErrorMessage
            };
            //return new DbResponse<TResult, TOutput>
            //{
            //    OutPutParameters = output,
            //    Data = model
            //};

        }

    }
}
