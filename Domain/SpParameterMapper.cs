﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Domain
{
    internal sealed class SpParameterMapper : IParameterMapper
    {
        DbCommand _cmd = null;

        public OutputParameter GetOurputParameters()
        {
            try
            {
                return new OutputParameter
                {
                    RetVal = (int)_cmd.Parameters["@RetVal"].Value,
                  //  DataType = (DataType)_cmd.Parameters["@DataType"].Value,
                    ErrorNumber = _cmd.Parameters["@ErrorNumber"].Value == DBNull.Value ? 0 : (int)_cmd.Parameters["@ErrorNumber"].Value,
                    ErrorMessage = _cmd.Parameters["@ErrorMessage"].Value == DBNull.Value ? string.Empty : (string)_cmd.Parameters["@ErrorMessage"].Value,
                  //  JsonOutput = _cmd.Parameters["@JsonOutput"].Value == DBNull.Value ? default(T) : ((string)_cmd.Parameters["@JsonOutput"].Value).To<T>()
                };
            }
            catch (Exception e)
            {
                throw new ApplicationException("Error Occured While reading Output Parameter of Stored Procedure.");
            }
        }

        public void AssignParameters(DbCommand command, object[] parameterValues)
        {
            if (parameterValues == null)
            {
                throw new ArgumentNullException();
            }
            if (parameterValues.Count() != 1)
            {
                throw new ArgumentException("Invalid Stored Procedure Parameter Count.");
            }

            _cmd = command;

            DbParameter[] parametersArray = new DbParameter[] {
                command.CreateParameter(),
                command.CreateParameter(),
                command.CreateParameter(),
                command.CreateParameter(),
                command.CreateParameter(),
                command.CreateParameter()
            };

            parametersArray[0].ParameterName = "@Json";
            parametersArray[0].Value = parameterValues[0].ToJson();

            parametersArray[1].ParameterName = "@RetVal";
            parametersArray[1].DbType = DbType.Int32;
            parametersArray[1].Direction = ParameterDirection.Output;
            parametersArray[1].Size = Int32.MaxValue;
            parametersArray[1].Value = DBNull.Value;

            parametersArray[2].ParameterName = "@DataType";
            parametersArray[2].DbType = DbType.Int32;
            parametersArray[2].Direction = ParameterDirection.Output;
            parametersArray[2].Size = Int32.MaxValue;
            parametersArray[2].Value = DBNull.Value;

            parametersArray[3].ParameterName = "@ErrorNumber";
            parametersArray[3].DbType = DbType.Int32;
            parametersArray[3].Direction = ParameterDirection.Output;
            parametersArray[3].Size = Int32.MaxValue;
            parametersArray[3].Value = DBNull.Value;

            parametersArray[4].ParameterName = "@ErrorMessage";
            parametersArray[4].DbType = DbType.String;
            parametersArray[4].Size = 2000;
            parametersArray[4].Direction = ParameterDirection.Output;
            parametersArray[4].Value = DBNull.Value;

            parametersArray[5].ParameterName = "@JsonOutput";
            parametersArray[5].DbType = DbType.String;
            parametersArray[5].Size = 8000;
            parametersArray[5].Direction = ParameterDirection.Output;
            parametersArray[5].Value = DBNull.Value;

            command.Parameters.AddRange(parametersArray);
        }
    }
}
