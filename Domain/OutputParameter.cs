﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class OutputParameter
    {
        public int RetVal { get; set; }
       public int ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }
       // public T JsonOutput { get; set; }
    }
}