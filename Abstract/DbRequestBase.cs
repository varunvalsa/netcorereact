﻿using System;

namespace Abstract
{
    public abstract class DbRequestBase <T>
    {
        public string ProcedureName { get; set; }
        public T InputJson { get; set; }

    }
}
