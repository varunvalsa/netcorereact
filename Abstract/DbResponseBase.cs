﻿using System;

namespace Abstract
{
    public class DbResponseBase<TResult> where TResult : class
    {
        public TResult Data { get; set; }
        public bool Status { get; set; }
        public int RetVal { get; set; }
        public string ResponseMessage { get; set; }
        public int ErrorNumber { get; set; }
    }
}
