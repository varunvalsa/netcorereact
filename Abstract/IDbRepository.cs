﻿using System.Threading.Tasks;

namespace Abstract
{
    public interface IDbRepository
    {
        Task<DbResponseBase<TResult>> GetResponseAsync<TResult,T>(DbRequestBase<T> request) where TResult : class;
    }

    
}