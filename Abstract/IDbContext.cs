﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstract
{
    public interface IDbContext
    {
        IDbRepository DbRepository { get; set; }
    }
}
